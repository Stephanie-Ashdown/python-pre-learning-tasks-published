def vowel_swapper(string):
    # ==============
    arr_string = list(string)
    a = 0
    e = 0
    i = 0
    o = 0
    o_ind = 0
    O_ind = 0
    u = 0
    
    for j in range(len(arr_string)):
        if arr_string[j].lower() == 'a':
            a += 1
            if a == 2:
                arr_string[j] = '4'
        elif arr_string[j].lower() == 'e':
            e += 1
            if e == 2:
                arr_string[j] = '3'
        elif arr_string[j].lower() == 'i':
            i += 1
            if i == 2:
                arr_string[j] = '!'
        elif arr_string[j].lower() == 'u':
            u += 1
            if u == 2:
                arr_string[j] = '|_|'
        elif arr_string[j] == 'o':
            o += 1
            if o == 2:
                o_ind = j
        elif arr_string[j] == 'O':
            o += 1
            if o == 2:
                O_ind = j
    
    if o_ind > 0:
        arr_string[o_ind] = 'ooo'
    elif O_ind > 0:
        arr_string[O_ind] = '000'
        
    
    new_string = ""
    return (new_string.join(arr_string))

    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
